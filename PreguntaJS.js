const conexion = require('./bd')

module.exports = app => {
    const connect = conexion

    app.post('/registro', (req, res) => {
        const cedula = req.body.cedula
        const apellidos = req.body.apellidos
        const nombres = req.body.nombres
        const direccion = req.body.direccion
        const semestre = req.body.semestre
        const correo = req.body.correo
        const paralelo = req.body.paralelo

        connect.query('insert into estudiantes SET ?', {
            cedula, apellidos, nombres, direccion, semestre, correo, paralelo
        }, (error, resultado) => {
            res.redirect('/registro')
        })
    })
}